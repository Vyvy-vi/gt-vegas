import asyncio

from settings.local_settings import TOKEN
from bot import client

def main():
    client.run(TOKEN)

main()
