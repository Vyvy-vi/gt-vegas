import pytest

from plugins.meme import MeMePlugin
from plugins.meme import DEFAULT_REPLY_NO_SUB


@pytest.fixture(scope="module")
def single_meme_plugin(mocker):
    return MeMePlugin('Fake Bot')


def test_default_no_sub_meme_command(single_meme_plugin):
    mocker.patch('MeMePlugin.bot.say')
    
