"""This COG supplies the base and foundation for the XP System.

REDIS_DATE_FORMAT: str
    A python datetime format for conversion.
LOAD_DATA_SQL: str
    SQL query to load users information into Redis.
SAVE_DATA_SQL: str
    Requires score, multiplier, multi_timeout and user id. Save a user to the DB, or if the user exists UPDATE the information.
TOP_DATA_SQL: str
    Requires a Guild ID and Offset (pagination). Returns 10 rows of user data from Guild.
PRUNE_DATA_SQL: str
    Requires a Guild ID. Returns all users info who are no longer in Guild.
PRUNE_DELETE_SQL: str
    Requires a User ID. Used in conjunction with PRUNE_DATA_SQL to delete users who have left the Guild.
XP_SAVE_CHAN: str
    The string name of a discord.Channel to print debug data to.
FIRST_SAVE: bool
    If the bot should skip the initial save loop.   

"""
import asyncio
import random
import datetime

from typing import Optional
from typing import Tuple

import discord
from discord.ext import commands
from discord.ext import tasks
from discord.utils import get

from utils import process_timeout
from .base import BasePlugin

from settings.local_settings import DEBUG
from settings.base import REDIS_ERR


SCORE_LOWER = 5
SCORE_UPPER = 10
GLOBAL_MULT = 1

REDIS_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

LOAD_DATA_SQL = "select * from discord_xp_userxp AS xp LEFT JOIN discord_user_discorduser AS users ON xp.user_id = users.id"
SAVE_DATA_SQL = "INSERT INTO discord_xp_userxp (score, multiplier, multiplier_timeout, user_id) VALUES (%s, %s, %s, %s) ON CONFLICT (user_id) DO UPDATE SET score=EXCLUDED.score;"
TOP_DATA_SQL = "select * from discord_xp_userxp AS xp LEFT JOIN discord_user_discorduser AS users ON xp.user_id = users.id WHERE users.discord_guild_id = %s ORDER BY xp.score DESC OFFSET %s ROWS FETCH NEXT 10 ROWS ONLY;"
PRUNE_DATA_SQL = "select * from discord_xp_userxp AS xp LEFT JOIN discord_user_discorduser AS users ON xp.user_id = users.id WHERE users.discord_guild_id = %s"
PRUNE_DELETE_SQL = "DELETE FROM discord_xp_userxp WHERE id = %s;"

XP_SAVE_CHAN = 'bot-spam-logging'

FIRST_SAVE = True


class XP_Plugin(BasePlugin):
    """This COG provides the base commands for the XP system"""
    def __init__(self, bot):
        """Run and set initial XP data"""
        super().__init__(bot)
        self.bot = bot
        self.background_save_last = None
        self.startup_load_last = None
        self.startup_load.start()
        self.background_save.start()

    @tasks.loop(hours=3.0)
    async def background_save(self):
        """Automatically runs the self._save() command every 3 hours.

        Uses
        ----
        - XP_SAVE_CHAN - Used as the default reply_channel.

        Notes
        -----
        - Updates `self.background_save_last` to `datetime.datetime.now()` for logging.

        """
        global FIRST_SAVE
        if FIRST_SAVE:
            print("First save, skipping...")
            FIRST_SAVE = False
            return

        for guild in self.bot.guilds:
            # loop through every guild the bot is in
            reply_channel = get(guild.text_channels, name=XP_SAVE_CHAN)
            if reply_channel:
                await self._save(guild.id, reply_channel)
            else:
                # Send warning message to first channel that XP_SAVE_CHAN is not set
                await guild.text_channels[0].send("Needs {} for XP module to function properly. Using this channel for now!".format(XP_SAVE_CHAN))
                await self._save(guild.id, guild.text_channels[0])
        self.background_save_last = datetime.datetime.now()

    @tasks.loop(minutes=1.0, count=1)
    async def startup_load(self):
        """Initally runs on COG load to get data fro Postgres to Redis.
        Generates a Redis Hash similar to the following:

        {guild_id}:score = {
            member_id: member_score,
            1234567890: 150,
            ...
        }

        Uses
        ----
        - self.bot.pg_pool -  for fetching data
        - self.bot.redis - for setting data
        - score_key - Redis key {guild_id}:score
        """
        print("*"*50)
        print("**    Startup_load Data")
        print("*"*50)
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(LOAD_DATA_SQL)
                users_xp = await cur.fetchall()
        for user in users_xp:
            # Save each users score to Redis hash {guild_id}:score member_id score
            guild_id = str(user[6])
            guild_key = guild_id  + ":score"
            member_id = user[7]
            await self.bot.redis.hsetnx(guild_key, member_id, user[1])
        self.startup_load_last = datetime.datetime.now()

    def _generate_score(self) -> int:
        """Private method to generate a new score to give toa user

        Uses
        ----
        - SCORE_LOWER
        - SCORE_UPPER
        - GLOBAL_MULT

        Returns
        -------
        multi_score: int
            The score to be given to the user
        """
        base_score = random.randint(SCORE_LOWER, SCORE_UPPER)
        multi_score = base_score * GLOBAL_MULT
        return multi_score

    async def _normalize_user_data(self, guild_id: str, member_id: str) -> Tuple[int, float, str]:
        """Private method to fetch a users data from Redis and make it readable.

        Parameters
        ----------
        guild_id: str
            A string representing the ID of a discord.Guild
        member_id: str
            A string representing the id of a discord.Member
            
        Uses
        ----
        - *Redis Keys*
        - {guild_id}:score - a dictionary of user scores indexed by member_id.
        - {guild_id}:{member_id}:multi - a float value to multiple a users new score by.
        - {guild_id}:{member_id}:cooldown - the a key set to EXPIRE when the user can gain new score.
        
        Returns
        -------
        typing.Tuple[int, float, str]
            Returns a tuple representing the Users Score, Users multiplier, and if the user is on cooldown.
        """
        guild_key = "{}:score".format(guild_id)
        multi_key = "{}:{}:multi".format(guild_id, member_id)
        cooldown_key = "{}:{}:cooldown".format(guild_id, member_id)

        user_score = await self.bot.redis.hget(guild_key, member_id, encoding="utf-8")
        user_multi = await self.bot.redis.get(multi_key, encoding="utf-8")
        cooldown = await self.bot.redis.get(cooldown_key, encoding="utf-8")
        if user_score == '0':
            user_score = 1
        if user_multi == '0':
            user_multi = 1.0

        return (user_score, user_multi, cooldown, )

    async def _save(self, guild_id: int, reply_channel: discord.TextChannel):
        """Private method to save a guilds user scores to postgres.

        Parameters
        ----------
        guild_id: int
        The Guild ID to save user scores from.
        reply_channel: discord.TextChannel
            If DEBUG is True reply channel will be used to log every save.

        Uses
        ----
        - {guild_id}:score
        - {guild_id}:{member_id}:multi
        - DEBUG - If True, sends additional logging messages
        - self._check_user_exists
        """
        key = "{}:score".format(guild_id)
        snapshot = await self.bot.redis.hgetall(key, encoding="utf-8")
        # hgetall returns a dictionary representing the redis instance.
        for member_id, score in snapshot.items():
            link_id = await self._check_user_exists(int(member_id), guild_id)
            mult_key = "{}:{}:multi".format(guild_id, member_id)
            multiplier = await self.bot.redis.get(mult_key, encoding="utf-8")
            ttl = await self.bot.redis.pttl(mult_key)

            if not multiplier:
                # Must be a float
                multiplier = 1.0
            if ttl == -2:
                # pttl returns -2 as a nil/None value
                ttl = 0

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    iscore = int(float(score))
                    message = await reply_channel.send("Saving {} with score {}".format(member_id, score))
                    await cur.execute(SAVE_DATA_SQL, (iscore, multiplier, ttl, link_id))

                    if DEBUG:
                        await reply_channel.send("Saved {} with score {}".format(member_id, score))

        await reply_channel.send("XP Save complete!")

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_message(self, message: discord.Message):
        """Checks every member message to determine if a user should recieve
        score or a level up.

        Parameters
        ----------
        message: discord.Message

        Uses
        ----
        - {guild_id}:score - check/apply users score
        - {guild_id}:{member_id}:cooldown - check if user can recieve score
        - self._normalize_user_data 
        - LeveledRolesPlugin.level_user - Static method to check if a user should level up
        """
        if isinstance(message.channel, discord.DMChannel):
            return
        if message.author.bot:
            # Ignore bots
            return
        guild_id = str(message.author.guild.id)
        member_id = str(message.author.id)
        score_key = guild_id + ":score"
        cooldown_key = guild_id + ":" + member_id + ":" + 'cooldown'
        channel = message.channel
        score = self._generate_score()
        now = datetime.datetime.now()

        # Check if the user has any registered score
        if await self.bot.redis.hexists(score_key, member_id):
            user_score, user_multi, on_cooldown = await self._normalize_user_data(guild_id, member_id)

            # Multiplier users score if they have multiplier
            if user_multi:
                score = int(float(score) * float(user_multi))
            user_score = int(float(user_score) + score)

            if not on_cooldown:
                # If user not on cooldown
                from .leveled_roles import LeveledRolesPlugin as leveled_roles
                await leveled_roles.level_user(self.bot, channel, message.author, user_score)
                # Check if user should level up
                await self.bot.redis.hset(score_key, member_id, user_score)
                # Set new user score
                await self.bot.redis.setnx(cooldown_key, str(now.strftime(REDIS_DATE_FORMAT)))
                # Set new cooldown
                await self.bot.redis.expire(cooldown_key, 120)
                # Set cooldown to expire in 2 minutes
        else:
            # If no registered score, setup new score in Redis
            await self.bot.redis.hsetnx(score_key, member_id, score)
            await self.bot.redis.setnx(cooldown_key, 'active')
            await self.bot.redis.expire(cooldown_key, 120)

    @commands.group()
    @commands.guild_only()
    async def xp(self, context):
        """Displays an embed showing self.background_save_last and self.startup_load_last
        """
        author = context.author
        if context.invoked_subcommand is None:
            background_save_last = self.background_save_last
            startup_load_last = self.startup_load_last
            if not background_save_last:
                background_save_last = "Hasn't Run"
            else:
                background_save_last.strftime("%H:%M %S EST")
            if not startup_load_last:
                startup_load_last = "Hasn't Run"
            else:
                startup_load_last.strftime("%H:%M %s EST")

            embed = discord.Embed(title="XP Information", color=author.top_role.color)
            embed.add_field(name="Background Save Last Run:", value=background_save_last)
            embed.add_field(name="Startup Load Last Run:", value=startup_load_last)
            await context.send(embed=embed)

    @xp.group()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def prune(self, context):
        """Prunes the server scoreboard by removing members no longer in server.
        O(n) where n is user in DB

        Uses
        ----
        - PRUNE_DATA_SQL
        - PRUNE_DELETE_SQL
        """
        prune_num = 0
        # Used to track how many members are removed
        with context.typing():
            # Display "Bot is typging..."
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(PRUNE_DATA_SQL, (context.guild.id, ))
                    prune_data = await cur.fetchall()
                    for user in prune_data:
                        if not get(context.guild.members, id=user[-1]):
                            # if user not in guild
                            prune_num += 1
                            await cur.execute(PRUNE_DELETE_SQL, (user[0], ))
                            # Remove user from db
                            await context.send("User <@!{}> removed from scoreboard!".format(user[-1]))
        await context.send("Prune Completed! {} users removed.".format(prune_num))


    @xp.group()
    @commands.guild_only()
    async def top(self, context, page: int = 1):
        """Displays top users on the scoreboard. Can be
        paginated through by providing a page number.

        """
        with context.typing():
            try:
                int(page)
            except:
                await context.send("Please provide a valid number!")
            page -= 1
            offset = page * 10
            # because 10 records are returned, offset is used
            # to represent how many records were previous to 
            # this set
            leaderboard_string = "```\n #  |  User\n" + \
            "____|_______________________\n"

            leaderboard_fmt = " {} | {} (score: {})\n"

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(TOP_DATA_SQL, (context.guild.id, offset, ))
                    leaderboard_data = await cur.fetchall()
            for n, member in enumerate(leaderboard_data):
                rank = 1+n+offset
                score = member[1]
                member_id = member[-1]
                member = get(context.guild.members, id=member_id)
                if member:
                    member_str = leaderboard_fmt.format(rank, member.name, score)
                else:
                    member_str = leaderboard_fmt.format(rank, member_id, score)
                leaderboard_string = leaderboard_string + member_str
            leaderboard_string = leaderboard_string + "```"
        # Generate the embed
        embed = discord.Embed(title="Leaderboard Page {}".format(page+1),
                              description=leaderboard_string,
                              color=context.message.author.color)
        embed.set_thumbnail(url=context.guild.icon_url)
        embed.set_footer(text="You cause also pass in a page! `v!xp top {}` for more!".format(page+1))
        await context.send(embed=embed)

    @xp.command()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def save(self, context):
        """A command to call self._save() for the current guild.

        Uses
        ----
        - self._save

        Notes
        -----
        - Uses the channel called in as the reply_channel by default
        """
        await context.send("Starting!")
        await self._save(context.guild.id, context.message.channel)

    @xp.command()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def add(self, context, user: discord.Member, xp_amount: int):
        """Adds xp_amount to user's score in Redis

        Uses
        ----
        - {guild_id}:score
        - self._normalize_user_data
        """
        user_id = user.id
        guild_id = context.message.guild.id
        guild_key = str(guild_id) + ":score"

        if await self.bot.redis.hexists(guild_key, user_id):
            user_score, user_multi, _ = await self._normalize_user_data(guild_id, user_id)
            if user_multi:
                xp_amount = int(float(xp_amount) * float(user_multi))
            user_score = int(int(user_score) + xp_amount)

            await self.bot.redis.hset(guild_key, user_id, user_score)

        else:
            await self.bot.redis.hset(guild_key, user_id, xp_amount)

        await context.send("{} was awarded {} XP!".format(user.mention, xp_amount))

    @xp.command()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def remove(self, context, user: discord.Member, xp_amount: int):
        """Removes xp_amount from user's score in Redis

        Uses
        ----
        - {guild_id}:score
        - self._normalize_user_data
        """
        user_id = user.id
        guild_id = context.message.guild.id
        guild_key = str(guild_id) + ":score"

        if await self.bot.redis.exists(guild_key):
            user_score, user_multi, _ = await self._normalize_user_data(guild_id, user_id)
            if user_multi:
                xp_amount = int(float(xp_amount) * float(user_multi))
            user_score = int(int(user_score) - xp_amount)

            await self.bot.redis.hset(guild_key, user_id, user_score)

            await context.send("{} XP was reomved from {}.".format(xp_amount, user.mention))

        else:
            await context.send("{} has no XP profile.".format(user.mention))

    @xp.command()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def multiplier(self, context, user: discord.Member, multiplier: float, timeout: Optional[str]):
        """Adds the multiplier to user's multi in Redis

        Parameters
        ----------
        user: discord.Member
            The Member to apply the multiplier to
        multiplier: float
            The floating point number to use as a multiplier when the user
            recieves score
        timeout: Optional[str]
            A "timeout" string representing how long the user should
            keep the multiplier
        
        Uses
        ---
        - self.bot.redis
        - REDIS_DATE_FORMAT
        - process_timeout
        - {guild_id}:{member_id}:multi
        """
        if not user:
            await context.send("Please provide a user!")
            return

        user_id = user.id
        guild_id = context.message.guild.id
        mult_key = "{}:{}:multi".format(guild_id, user_id)

        await self.bot.redis.set(mult_key, multiplier)

        if timeout:
            # Convert timeout to seconds
            now = datetime.datetime.now()
            removal_time, _ = process_timeout(timeout)
            timeout_delta = removal_time - now
            removal_time = removal_time.strftime(REDIS_DATE_FORMAT)
            # apply redis.expire, which will automatically remove the key
            await self.bot.redis.expire(mult_key, timeout_delta.total_seconds())
        else:
            # Warn the user no timeout was provided
            await context.send("{} did not add a timeout! This multiplier will be permanent!".format(context.author.mention))
            removal_time = "Permanent"

        await context.send("{} multipler was set to {}. (expires {})".format(
            user.mention,
            multiplier,
            removal_time)
        )

    @xp.command(aliases=['ud',])
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def user_dump(self, context, user: Optional[discord.Member]):
        """Displays an embed with information about the users XP in VEGAS.

        Parameters
        ----------
        user: Optional[discord.Member]
            The member to view information about.

        Uses
        ----
        - self.bot.redis
        - self._normalize_user_data
        - {guild_id}:{member_id}:multi
        """
        guild = context.guild
        if user:
            member = context.message.mentions[0]
        else:
            member = context.message.author
        user_score, user_multi, on_cooldown = await self._normalize_user_data(guild.id, member.id)
        embed = discord.Embed(title="{} XP Profile".format(member.name),
                              color=member.top_role.color)
        if user_multi:
            mult_key = "{}:{}:multi".format(guild.id, member.id)
            ttl = await self.bot.redis.pttl(mult_key)
            if ttl == -1:
                ttl = "∞"
            else:
                ttl =  str(int(ttl / 1000 / 60)) + " minutes"

            embed.add_field(name="XP Multiplier:", value=user_multi, inline=True)
            embed.add_field(name="XP Timeout:", value=ttl, inline=True)

        if user_multi and on_cooldown:
            embed.add_field(name="-", value="-", inline=False)

        if on_cooldown:
            cooldown_key = str(guild.id) + ":" + str(member.id) + ":cooldown"
            ttl = await self.bot.redis.pttl(cooldown_key)
            ttl =  str(int(ttl / 1000)) + " seconds"

            embed.add_field(name="Cooldown:", value=ttl, inline=True)
            embed.add_field(name="Active:", value="X", inline=True)
        embed.add_field(name="User Score:", value=user_score, inline=False)
        await context.send(embed=embed)


    @xp.command()
    @commands.guild_only()
    @commands.has_any_role('Mods')
    async def key_del(self, context, key: str):
        """Allows a mod to delete a key from Redis

        Parameters
        ----------
        key: str
            A Redis key to delete

        Uses
        ----
        - self.bot.redis
        """
        await self.bot.redis.delete(key)
        await context.send("Deleted {}".format(key))

def setup(bot):
    """Add XP_Plugin to bot if redis is available

    Uses
    ----
    - bot.redis
    """
    if not hasattr(bot, 'redis'):
        print("*"*50)
        print("xp.py: " + REDIS_ERR)
        print("*"*50)
        return
    bot.add_cog(XP_Plugin(bot))
