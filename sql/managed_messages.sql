--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_managed_message_managedembed; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_managed_message_managedembed (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    color character varying(20) NOT NULL,
    description text NOT NULL,
    image character varying(255) NOT NULL,
    thumbnail character varying(255) NOT NULL,
    footer text NOT NULL,
    messages_id integer NOT NULL
);


ALTER TABLE public.discord_managed_message_managedembed OWNER TO postgres;

--
-- Name: discord_managed_message_managedembed_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_managed_message_managedembed_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_managed_message_managedembed_id_seq OWNER TO postgres;

--
-- Name: discord_managed_message_managedembed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_managed_message_managedembed_id_seq OWNED BY public.discord_managed_message_managedembed.id;


--
-- Name: discord_managed_message_managedembed id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembed ALTER COLUMN id SET DEFAULT nextval('public.discord_managed_message_managedembed_id_seq'::regclass);


--
-- Name: discord_managed_message_managedembed discord_managed_message_managedembed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembed
    ADD CONSTRAINT discord_managed_message_managedembed_pkey PRIMARY KEY (id);


--
-- Name: discord_managed_message_managedembed_messages_id_3156968a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedembed_messages_id_3156968a ON public.discord_managed_message_managedembed USING btree (messages_id);


--
-- Name: discord_managed_message_managedembed discord_managed_mess_messages_id_3156968a_fk_discord_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembed
    ADD CONSTRAINT discord_managed_mess_messages_id_3156968a_fk_discord_m FOREIGN KEY (messages_id) REFERENCES public.discord_managed_message_managedmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_managed_message_managedembed; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_managed_message_managedembed TO user;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_managed_message_managedembedfields; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_managed_message_managedembedfields (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text NOT NULL,
    embed_id integer NOT NULL,
    inline boolean NOT NULL
);


ALTER TABLE public.discord_managed_message_managedembedfields OWNER TO postgres;

--
-- Name: discord_managed_message_managedembedfields_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_managed_message_managedembedfields_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_managed_message_managedembedfields_id_seq OWNER TO postgres;

--
-- Name: discord_managed_message_managedembedfields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_managed_message_managedembedfields_id_seq OWNED BY public.discord_managed_message_managedembedfields.id;


--
-- Name: discord_managed_message_managedembedfields id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembedfields ALTER COLUMN id SET DEFAULT nextval('public.discord_managed_message_managedembedfields_id_seq'::regclass);


--
-- Name: discord_managed_message_managedembedfields discord_managed_message_managedembedfields_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembedfields
    ADD CONSTRAINT discord_managed_message_managedembedfields_pkey PRIMARY KEY (id);


--
-- Name: discord_managed_message_managedembedfields_embed_id_3b0583e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedembedfields_embed_id_3b0583e6 ON public.discord_managed_message_managedembedfields USING btree (embed_id);


--
-- Name: discord_managed_message_managedembedfields discord_managed_mess_embed_id_3b0583e6_fk_discord_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedembedfields
    ADD CONSTRAINT discord_managed_mess_embed_id_3b0583e6_fk_discord_m FOREIGN KEY (embed_id) REFERENCES public.discord_managed_message_managedembed(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_managed_message_managedembedfields; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_managed_message_managedembedfields TO user;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_managed_message_managedmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_managed_message_managedmessage (
    id integer NOT NULL,
    channel_id bigint NOT NULL,
    message_id bigint,
    content text NOT NULL
);


ALTER TABLE public.discord_managed_message_managedmessage OWNER TO postgres;

--
-- Name: discord_managed_message_managedmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_managed_message_managedmessage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_managed_message_managedmessage_id_seq OWNER TO postgres;

--
-- Name: discord_managed_message_managedmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_managed_message_managedmessage_id_seq OWNED BY public.discord_managed_message_managedmessage.id;


--
-- Name: discord_managed_message_managedmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedmessage ALTER COLUMN id SET DEFAULT nextval('public.discord_managed_message_managedmessage_id_seq'::regclass);


--
-- Name: discord_managed_message_managedmessage discord_managed_message_managedmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedmessage
    ADD CONSTRAINT discord_managed_message_managedmessage_pkey PRIMARY KEY (id);


--
-- Name: discord_managed_message_managedmessage_channel_id_889475b7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedmessage_channel_id_889475b7 ON public.discord_managed_message_managedmessage USING btree (channel_id);


--
-- Name: discord_managed_message_managedmessage_message_id_d17d8f97; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedmessage_message_id_d17d8f97 ON public.discord_managed_message_managedmessage USING btree (message_id);


--
-- Name: TABLE discord_managed_message_managedmessage; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_managed_message_managedmessage TO user;


--
-- PostgreSQL database dump complete
--

